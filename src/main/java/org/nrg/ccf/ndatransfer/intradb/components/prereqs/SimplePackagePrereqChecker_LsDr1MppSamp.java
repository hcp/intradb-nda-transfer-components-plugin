package org.nrg.ccf.ndatransfer.intradb.components.prereqs;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.NdaPackagePrereqCheckerI;
import org.nrg.ccf.ndatransfer.anno.NdaPackagePrereqChecker;
import org.nrg.ccf.ndatransfer.components.simple.SimplePackagePrereqChecker;
import org.nrg.ccf.ndatransfer.utils.NdaTransferConfigUtils;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@NdaPackagePrereqChecker
public class SimplePackagePrereqChecker_LsDr1MppSamp extends SimplePackagePrereqChecker {
	
	final NdaTransferConfigUtils _configUtils = XDAT.getContextService().getBean(NdaTransferConfigUtils.class);
	final Map<String,NdaPackagePrereqCheckerI> _cache = new HashMap<>();
	
	final static String[] MPP_ARRAY = { 
			"HCA8793607", "HCA7410559", "HCA7841485", "HCA6451366", "HCA7226566", "HCA8066676", "HCA6909286", "HCA7885102", 
			"HCA7888007", "HCA8532174", "HCA7910680", "HCA7717484", "HCA8358285", "HCA6670479", "HCA9521073", "HCA7130957", 
			"HCA7495593", "HCA9157179", "HCA7251969", "HCA9392997", "HCA7898818", "HCA6706373", "HCA9455288", "HCA7943190", 
			"HCA8854398", "HCA7492991", "HCA9662495", "HCA9319684", "HCA9535488", "HCA7175272", "HCA7046968", "HCA9023865", 
			"HCA6680987", "HCA6877300", "HCA7627786", "HCA6910372", "HCA8626082", "HCA7030751", "HCA9953406", "HCA7825487", 
			"HCA7962902", "HCA7884605", "HCA6640672", "HCA6320048", "HCA9839307", "HCA8441777", "HCA9096690", "HCA7467588", 
			"HCA6552069", "HCA6276475", "HCA9703079", "HCA8795813", "HCA9761497", "HCA7214357", "HCA6618679", "HCA7317367", 
			"HCA9037876", "HCA9546594", "HCA9646699", "HCA7544883", "HCA8417376", "HCA7434775", "HCA8154168", "HCA8514374", 
			"HCA9601374", "HCA6930782", "HCA6685492", "HCA6374576", "HCA9708190", "HCA7950086", "HCA9369902", "HCA6700664", 
			"HCA8883911", "HCA8980404", "HCA9464996", "HCA6436774", "HCA6954998", "HCA6280668", "HCA8244977", "HCA6166973", 
			"HCA8794407", "HCA8934497", "HCA6876095", "HCA9559806", "HCA9688312", "HCA6290368", "HCA7670686", "HCA7017961", 
			"HCA8697005", "HCA9974010", "HCA8959211", "HCA8623581", "HCA9094484", "HCA6686191", "HCA7397391", "HCA7453981", 
			"HCA6301751", "HCA7703372", "HCA8074372", "HCA6461066", "HCA6771081", "HCA7319472", "HCA9880405", "HCA9436688", 
			"HCA6367781", "HCA7415973", "HCA7497395", "HCA8405975", "HCA7510058", "HCA6753079", "HCA9539800", "HCA8869513", 
			"HCA9234979", "HCA6451770", "HCA8796916", "HCA7855800", "HCA8000040", "HCA8904185", "HCA6283371", "HCA8908092", 
			"HCA6318465", "HCA9735294", "HCA9365287", "HCA6317665", "HCA6125050", "HCA8253978", "HCA8134667", "HCA6302349", 
			"HCA9407681", "HCA6086369", "HCA8402969", "HCA8206666", "HCA9086990", "HCA7296183", "HCA7980095", "HCA9787415", 
			"HCA6671380", "HCA9090779", "HCA6582684", "HCA9086283", "HCA8874304", "HCA8794912", "HCA8724991", "HCA7567693", 
			"HCA9300764", "HCA6682688", "HCA7030549", "HCA9155680", "HCA9449900", "HCA7688706", "HCA7982706", "HCA9914799", 
			"HCA8937807", "HCA8867610", "HCA9655599", "HCA7302657", "HCD1010311", "HCD0102412", "HCD2888789", "HCD0165941", 
			"HCD2370952", "HCD2607452", "HCD2926973", "HCD0383143", "HCD2743157", "HCD0102210", "HCD2828569", "HCD1047233", 
			"HCD2751964", "HCD1876272", "HCD2884377", "HCD0040113", "HCD2236140", "HCD0148537", "HCD2523345", "HCD0154431", 
			"HCD2812352", "HCD0494354", "HCD2692671", "HCD0367448", "HCD1647863", "HCD0001305", "HCD2913459", "HCD0135831", 
			"HCD2863975", "HCD0247438", "HCD2955374", "HCD1001916", "HCD1187653", "HCD1479969", "HCD2669575", "HCD0891566", 
			"HCD1029837", "HCD1694569", "HCD2311633", "HCD0947365", "HCD2721147", "HCD0641341", "HCD2990073", "HCD0393550", 
			"HCD2301428", "HCD0555146", "HCD2833360", "HCD0124725", "HCD2856170", "HCD1017729", "HCD2400026", "HCD0394754", 
			"HCD1464653", "HCD0330021", "HCD2574564", "HCD0130821", "HCD2715152", "HCD0119429", "HCD2748672", "HCD0504735", 
			"HCD2286155", "HCD1363546", "HCD2256651", "HCD0574756", "HCD2939275", "HCD0267747", "HCD1881164", "HCD0392649", 
			"HCD2930055", "HCD0812241", "HCD2737869", "HCD1711240", "HCD1762661", "HCD0225630", "HCD2957277", "HCD0041822", 
			"HCD1633751", "HCD0236130", "HCD2797584", "HCD0584860", "HCD2982579", "HCD0857566", "HCD1342033", "HCD0042420", 
			"HCD2001719", "HCD1131222", "HCD2724456", "HCD0031617", "HCD2105125", "HCD1285956", "HCD2530140", "HCD1663053", 
			"HCD1925461", "HCD1712444", "HCD1631646", "HCD0692964", "HCD0788674", "HCD0311320", "HCD2801852", "HCD0259243", 
			"HCD2841965", "HCD1128738", "HCD2864270", "HCD0378150", "HCD2860262", "HCD2045638", "HCD2136540", "HCD1237844" 
								};
	final static List<String> MPP_SAMPLE = Arrays.asList(MPP_ARRAY);
	
	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user, DataPackageDefinition packageInfo) {
		try {
		    super.checkPrereqs(statusEntity, user, packageInfo);
			final String subjectLabel = statusEntity.getEntityLabel().replaceFirst("_.*$","");
			if (!MPP_SAMPLE.contains(subjectLabel)) {
				final String prereqsInfo = (statusEntity.getPrereqsInfo() != null) ? statusEntity.getPrereqsInfo() : "";
				if (statusEntity.getPrereqs()) {
					statusEntity.setPrereqsTime(new Date());
				}
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsInfo(prereqsInfo + "<li>NOT MDD:  Subject is not an MDD-selected subject for the first (03/2019) data release</li>");
			}
		} catch (Exception e) {
			log.debug(e.toString());
		}
	}

}
