package org.nrg.ccf.ndatransfer.intradb.components.matchers;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.nrg.ccf.ndatransfer.abst.Matcher;
import org.nrg.ccf.ndatransfer.anno.FileMatcher;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;

import com.google.common.collect.ImmutableList;

@FileMatcher
public class BatchTxtRenameMatcher extends Matcher implements FileMatcherI {

	public BatchTxtRenameMatcher() {
		super();
	}

	@Override
	public List<String> configurationYaml() {
		return ImmutableList.of();
	}

	@Override
	public boolean evaluateFile(final String resourceRootPath, File file) {
		return (file.isFile() && (file.getName().equals("batch.txt") ||
				file.getName().contains("run_qunex.sh")));
	}

	@Override
	public String getCanonicalClassname() {
		return this.getClass().getCanonicalName();
	}

	@Override
	public boolean validateFileMatches(Collection<File> arg0) {
		return true;
	}

	@Override
	public String toString() {
		return "BatchTxtRenameMatcher []";
	}

}
