package org.nrg.ccf.ndatransfer.intradb.components.prereqs;

import java.util.Date;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.anno.NdaPackagePrereqChecker;
import org.nrg.ccf.ndatransfer.components.simple.SimplePackagePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@NdaPackagePrereqChecker
public class SimplePackagePrereqChecker_LsDr1NoMppSamp extends SimplePackagePrereqChecker {
	
	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user, DataPackageDefinition packageInfo) {
		try {
			super.checkPrereqs(statusEntity, user, packageInfo);
			final String subjectLabel = statusEntity.getEntityLabel().replaceFirst("_.*$","");
			// Note:  Cannot extend SimplePackagePrereqChecker_LsDr1NoMppSamp because we have to call super and don't want that one called
			// We'll just use the MPP_SAMPLE list from that class
			if (SimplePackagePrereqChecker_LsDr1MppSamp.MPP_SAMPLE.contains(subjectLabel)) {
				final String prereqsInfo = (statusEntity.getPrereqsInfo() != null) ? statusEntity.getPrereqsInfo() : ""; 
				if (statusEntity.getPrereqs()) {
					statusEntity.setPrereqsTime(new Date());
				}
				statusEntity.setPrereqs(false);;
				statusEntity.setPrereqsInfo(prereqsInfo + "<li>MDD SUBJECT:  Subject is an MDD-selected subject for the first (03/2019) data release</li>");
			}
		} catch (Exception e) {
			log.debug(e.toString());
		}
	}

}
