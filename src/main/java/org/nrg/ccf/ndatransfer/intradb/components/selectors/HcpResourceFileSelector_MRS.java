package org.nrg.ccf.ndatransfer.intradb.components.selectors;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.anno.FileSelector;
import org.nrg.ccf.ndatransfer.components.selectors.ResourceFileSelector;
import org.nrg.ccf.ndatransfer.constants.MatchOperator;
import org.nrg.ccf.ndatransfer.constants.ResourceType;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

@FileSelector
public class HcpResourceFileSelector_MRS extends ResourceFileSelector {

	public HcpResourceFileSelector_MRS() {
		super();
	}

	public HcpResourceFileSelector_MRS(ResourceType resourceType, String matchRegex, String excludeRegex, MatchOperator matchOperator, Integer matchCount) {
		super(resourceType, matchRegex, excludeRegex, matchOperator, matchCount);
	}
	
	@Override
	public Map<String,List<File>> generateFileMap(DataPackageDefinition packageInfo, XnatImagesessiondataI exp, UserI user) {
		final Map<String, List<File>> returnMap = new LinkedHashMap<>();
		if (resourceType.equals(ResourceType.SESSION)) {
			final List<XnatAbstractresourceI> sessionResources = exp.getResources_resource();
			final String expLbl = exp.getLabel();
			for (final XnatAbstractresourceI resource : findMatchingResources(sessionResources)) {
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				for (final FileMatcherI fileMatcher : getFileMatchers()) {
					final Map<String, List<File>> matches = findFileMatches((XnatResourcecatalog)resource,fileMatcher,user);
					for (Entry<String, List<File>> entry : matches.entrySet()) {
						// First, we handle unproc resources differently.
						boolean isUnproc = false;
						for (final File cFile : entry.getValue()) {
							if (!cFile.getAbsolutePath().matches("^.*/RESOURCES/[^/]*_unproc/.*$")) {
								continue;
							}
							//final String resourceLbl = cFile.getAbsolutePath().replaceFirst("^.*" + expLbl + "/RESOURCES/", "").replaceFirst("_unproc\\/.*$",""); 
							//final StringBuilder rPathSb = new StringBuilder(expLblArr[0]);
							//	rPathSb.append("/unprocessed/").append(expLblArr[1]).append("_").append(expLblArr[2])
							final StringBuilder rPathSb = new StringBuilder(expLbl);
							// Put everything in one directory under "MR SPECTROSCOPY_DATA"
							rPathSb.append("/unprocessed/").append("MR_SPECTROSCOPY_DATA").append("/").append(entry.getKey().replaceFirst("^.*[/]", ""));
							final String rbPathSbStr = rPathSb.toString();
							if (!returnMap.containsKey(rbPathSbStr)) {
								returnMap.put(rbPathSbStr, new ArrayList<File>());
							}
							returnMap.get(rbPathSbStr).add(cFile);
							isUnproc = true;
						}
						if (isUnproc) {
							continue;
						}
						// Now, handle all other (non-unproc) resources
						final String newKey = entry.getKey().replaceFirst("ProcessingInfo/processing/logs",
								"ProcessingInfo/QuNex/processing/logs")
								// Moved processing/logs stuff first to logs/QuNex.  Then move other stuff under ProcessingInfo/.
								// Moved processing/logs stuff first to logs/QuNex.  Then move other stuff under ProcessingInfo/.
								//.replaceFirst("ProcessingInfo/","logs/QuNex/")
								;
						if (!returnMap.containsKey(newKey)) {
							returnMap.put(newKey, new ArrayList<File>());
						}
						returnMap.get(newKey).addAll(entry.getValue());
					}
				}
			}
		} else {
			returnMap.putAll(super.generateFileMap(packageInfo, exp, user));
		}
		return returnMap;
	}

}
