package org.nrg.ccf.ndatransfer.intradb.components.matchers;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.nrg.ccf.ndatransfer.abst.Matcher;
import org.nrg.ccf.ndatransfer.anno.FileMatcher;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;

import com.google.common.collect.ImmutableList;

@FileMatcher
public class PreprocPhysioEvMatcher extends Matcher implements FileMatcherI {

	public PreprocPhysioEvMatcher() {
		super();
	}

	@Override
	public List<String> configurationYaml() {
		return ImmutableList.of();
	}

	@Override
	public boolean evaluateFile(final String resourceRootPath, File file) {
		final String relativePath = file.getPath().replace(resourceRootPath + File.separator, "");
		return (file.isFile() && (relativePath.contains("PHYSIO/") || relativePath.contains("EVs/")));
	}

	@Override
	public String getCanonicalClassname() {
		return this.getClass().getCanonicalName();
	}

	@Override
	public boolean validateFileMatches(Collection<File> arg0) {
		return true;
	}

	@Override
	public String toString() {
		return "PreprocPhysioEvMatcher []";
	}
	
}
