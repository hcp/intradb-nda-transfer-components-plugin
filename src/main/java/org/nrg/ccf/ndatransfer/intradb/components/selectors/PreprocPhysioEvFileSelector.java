package org.nrg.ccf.ndatransfer.intradb.components.selectors;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.anno.FileSelector;
import org.nrg.ccf.ndatransfer.components.selectors.ResourceFileSelector;
import org.nrg.ccf.ndatransfer.constants.MatchOperator;
import org.nrg.ccf.ndatransfer.constants.ResourceType;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@FileSelector
public class PreprocPhysioEvFileSelector extends ResourceFileSelector {

	public PreprocPhysioEvFileSelector() {
		super();
	}

	public PreprocPhysioEvFileSelector(ResourceType resourceType, String matchRegex, String excludeRegex, MatchOperator matchOperator, Integer matchCount) {
		super(resourceType, matchRegex, excludeRegex, matchOperator, matchCount);
	}
	
	@Override
	public Map<String,List<File>> generateFileMap(DataPackageDefinition packageInfo, XnatImagesessiondataI exp, UserI user) {
		final Map<String, List<File>> returnMap = new LinkedHashMap<>();
		if (resourceType.equals(ResourceType.SESSION)) {
			final List<XnatAbstractresourceI> sessionResources = exp.getResources_resource();
			final String expLbl = exp.getLabel();
			for (final XnatAbstractresourceI resource : findMatchingResources(sessionResources)) {
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				for (final FileMatcherI fileMatcher : getFileMatchers()) {
					final Map<String, List<File>> matches = findFileMatches((XnatResourcecatalog)resource,fileMatcher,user);
					for (Entry<String, List<File>> entry : matches.entrySet()) {
						// First, we handle unproc resources differently.
						for (final File cFile : entry.getValue()) {
							if (!cFile.getAbsolutePath().matches("^.*/RESOURCES/[^/]*_unproc/.*$")) {
								continue;
							}
							final String resourceLbl = cFile.getAbsolutePath().replaceFirst("^.*" + expLbl + "/RESOURCES/", "").replaceFirst("_unproc\\/.*$",""); 
							//final StringBuilder rPathSb = new StringBuilder(expLblArr[0]);
							//	rPathSb.append("/unprocessed/").append(expLblArr[1]).append("_").append(expLblArr[2])
							final StringBuilder rPathSb = new StringBuilder(expLbl);
							rPathSb.append("/MNINonLinear/Results/").append(resourceLbl).append("/").append(entry.getKey());
							final String rbPathSbStr = rPathSb.toString().replace("LINKED_DATA/", "").replace("PHYSIO/", "").replace("PSYCHOPY/", "");
							if (!returnMap.containsKey(rbPathSbStr)) {
								returnMap.put(rbPathSbStr, new ArrayList<File>());
							}
							returnMap.get(rbPathSbStr).add(cFile);
						}
					}
				}
			}
		} 
		return returnMap;
	}

}
