package org.nrg.ccf.ndatransfer.intradb.components.selectors;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.anno.FileSelector;
import org.nrg.ccf.ndatransfer.components.selectors.ResourceFileSelector;
import org.nrg.ccf.ndatransfer.constants.MatchOperator;
import org.nrg.ccf.ndatransfer.constants.ResourceType;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@FileSelector
public class BatchTxtRenameFileSelector extends ResourceFileSelector {

	public BatchTxtRenameFileSelector() {
		super();
	}

	public BatchTxtRenameFileSelector(ResourceType resourceType, String matchRegex, String excludeRegex, MatchOperator matchOperator, Integer matchCount) {
		super(resourceType, matchRegex, excludeRegex, matchOperator, matchCount);
	}
	
	@Override
	public Map<String,List<File>> generateFileMap(DataPackageDefinition packageInfo, XnatImagesessiondataI exp, UserI user) {
		final Map<String, List<File>> returnMap = new LinkedHashMap<>();
		if (resourceType.equals(ResourceType.SESSION)) {
			final List<XnatAbstractresourceI> sessionResources = exp.getResources_resource();
			final String expLbl = exp.getLabel();
			for (final XnatAbstractresourceI resource : findMatchingResources(sessionResources)) {
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				for (final FileMatcherI fileMatcher : getFileMatchers()) {
					final Map<String, List<File>> matches = findFileMatches((XnatResourcecatalog)resource,fileMatcher,user);
					for (Entry<String, List<File>> entry : matches.entrySet()) {
						// First, we handle unproc resources differently.
						String resourceLbl = "";
						boolean hasMatch = false;
						for (final File cFile : entry.getValue()) {
							if (!(cFile.getAbsolutePath().matches("^.*/ProcessingInfo/processing/batch.txt$") ||
									cFile.getAbsolutePath().matches("^.*/ProcessingInfo/run_qunex.sh$") ||
									cFile.getAbsolutePath().matches("^.*/ProcessingInfo/.*/run_qunex.sh.*log$"))
									) {
								continue;
							}
							hasMatch = true;
							resourceLbl = cFile.getAbsolutePath().replaceFirst("^.*" + expLbl + "/RESOURCES/", "").replaceFirst("_unproc\\/.*$","")
									.replaceFirst("_proc.*$","").replaceFirst("/.*$", "");
						}
						if (!hasMatch) {
							continue;
						}
						final String newKey = entry.getKey().replaceFirst("batch.txt", "batch_" + resourceLbl + ".txt")
								.replaceFirst("run_qunex.sh", "run_qunex_" + resourceLbl + ".sh")
								;
						if (!returnMap.containsKey(newKey)) {
							returnMap.put(newKey, new ArrayList<File>());
						}
						returnMap.get(newKey).addAll(entry.getValue());
					}
				}
			}
		} 
		return returnMap;
	}
	
}
