package org.nrg.ccf.ndatransfer.intradb.components.selectors;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.anno.FileSelector;
import org.nrg.ccf.ndatransfer.components.selectors.ScanResourceFileSelector;
import org.nrg.ccf.ndatransfer.constants.MatchOperator;
import org.nrg.ccf.ndatransfer.constants.ResourceType;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@FileSelector
@Slf4j
public class HcpImage03ScanFileSelector extends ScanResourceFileSelector {
	
	public HcpImage03ScanFileSelector() {
		super();
	}

	public HcpImage03ScanFileSelector(ResourceType resourceType, String matchRegex, String excludeRegex, MatchOperator matchOperator, Integer matchCount) {
		super(resourceType, matchRegex, excludeRegex, matchOperator, matchCount);
	}
	
	@Override
	public Map<String, List<File>> generateFileMap(DataPackageDefinition packageInfo, XnatImagesessiondataI session, XnatImagescandataI scan, UserI user) {
		final Map<String, List<File>> returnMap = new LinkedHashMap<>();
		for (final XnatAbstractresourceI resource : session.getResources_resource()) {
			if (!(resource instanceof XnatResourcecatalog) || !resource.getLabel().matches("^.*_unproc")) {
				continue;
			}
			// We're not publishing the 4e_RMS resource
			final String resourceLabel = resource.getLabel();
			if (resourceLabel.matches("^.*_4e_RMS.*$")) {
				continue;
			}
			for (final FileMatcherI fileMatcher : getFileMatchers()) {
				final Map<String, List<File>> matches = findFileMatches((XnatResourcecatalog)resource,fileMatcher,user);
				for (Entry<String, List<File>> entry : matches.entrySet()) {
					final StringBuilder sb = new StringBuilder(session.getLabel());
					sb.append("/unprocessed");
					if (!resourceLabel.equalsIgnoreCase(CommonConstants.NIFTI_RESOURCE)) {
						sb.append("/");
						sb.append(resourceLabel.replaceFirst("_unproc", ""));
					}
					final String pathStr = sb.toString();
						for (final File entryFile : entry.getValue()) {
							final Path entryFileP = Paths.get(entryFile.toURI());
							if (Files.isSymbolicLink(entryFileP)) {
								Path linkPath;
								try {
									linkPath = Files.readSymbolicLink(entryFileP);
									final String linkPathStr = linkPath.toString();
									// Let's only include files from the current scan and also the SBRef scan
									if (!(linkPathStr.contains("/" + scan.getId() + "/") || linkPathStr.contains(scan.getSeriesDescription() + "_SBRef"))) {
										continue;
									}
									if (linkPathStr.contains("LINKED_DATA")) {
										//// MRH: 2020/07/29.  Since mp4 files are being released we'll remove this restriction.  
										//// Restrictions can be supplied at the configuration stage.
										//// Let's only send physio files and EVs to avoid sending things we shouldn't (e.g. mp4 files that aren't ready).
										//if (!(linkPathStr.matches("^.*/Physio[^/]*$") || linkPathStr.matches("^.*/EVs/.*$"))) {
										//	continue;
										//}
									}
								} catch (IOException e) {
									log.debug("Couldn't read symbolic link:  " + entryFile.getPath());
								}
							} else if (!entryFile.getName().contains(scan.getSeriesDescription())) {
								continue;
							}
							final StringBuilder fpSb = new StringBuilder(pathStr);
							fpSb.append("/");
							fpSb.append(entry.getKey());
							final String finalPathStr = fpSb.toString();
							if (!returnMap.containsKey(finalPathStr)) {
								returnMap.put(finalPathStr, new ArrayList<File>());
							}
							returnMap.get(finalPathStr).add(entryFile);
					}
				}
			}
		}
		return returnMap;
	}
	
}
